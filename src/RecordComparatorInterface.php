<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-record-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Record;

use Stringable;

/**
 * RecordComparatorInterface interface file.
 * 
 * This interface specifies a comparator for record objects.
 * 
 * @author Anastaszor
 */
interface RecordComparatorInterface extends Stringable
{
	
	/**
	 * Compares two records and generates a number that is representative of
	 * the distance between the two objects. If the distance is zero, then
	 * the two objects are equals, and distance more than zero means there
	 * exists differences between the objects.
	 * 
	 * @param RecordInterface $record1
	 * @param RecordInterface $record2
	 * @return float the distance
	 */
	public function compare(RecordInterface $record1, RecordInterface $record2) : float;
	
}
