<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-record-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Record;

use InvalidArgumentException;
use Stringable;

/**
 * RecordInterface interface file.
 * 
 * This record represents a generic object that may store basic information.
 * 
 * @author Anastaszor
 */
interface RecordInterface extends Stringable
{
	
	/**
	 * Gets the namespace of this object.
	 * 
	 * @return string
	 */
	public function getNamespace() : string;
	
	/**
	 * Gets the classname of this object.
	 * 
	 * @return string
	 */
	public function getClassname() : string;
	
	/**
	 * Gets the id of the object.
	 * 
	 * @return string
	 */
	public function getIdentifier() : string;
	
	/**
	 * Gets whether this object is allowed to have the key value.
	 * 
	 * @param string $key
	 * @return boolean
	 */
	public function isAllowed(string $key) : bool;
	
	/**
	 * Gets whether this object has the value for the given key.
	 * 
	 * @param string $key
	 * @return boolean
	 */
	public function hasValue(string $key) : bool;
	
	/**
	 * Sets the value from the given key.
	 * 
	 * @param string $key
	 * @return string
	 * @throws InvalidArgumentException if the key does not have
	 */
	public function getValue(string $key) : string;
	
	/**
	 * Gets the value from the given key.
	 * 
	 * @param string $key
	 * @param string $value
	 * @return boolean whether the value is accepted
	 * @throws InvalidArgumentException if the key is not allowed to have value
	 */
	public function setValue(string $key, string $value) : bool;
	
}
