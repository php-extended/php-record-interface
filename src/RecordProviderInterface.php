<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-record-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Record;

use InvalidArgumentException;
use Iterator;
use Stringable;

/**
 * RecordProviderInterface class file.
 * 
 * This interface specifies how records are retrieved from their basic
 * properties.
 * 
 * @author Anastaszor
 */
interface RecordProviderInterface extends Stringable
{
	
	/**
	 * Gets all the records for the given namespace and classname.
	 * 
	 * @param ?string $namespace
	 * @param ?string $classname
	 * @return Iterator<RecordInterface>
	 */
	public function getAllRecords(?string $namespace, ?string $classname) : Iterator;
	
	/**
	 * Gets whether the record with the given namespace, classname, identifier
	 * exists and may be retrieved with the self::getRecord() method.
	 * 
	 * @param string $namespace
	 * @param string $classname
	 * @param string $identifier
	 * @return boolean
	 */
	public function hasRecord(string $namespace, string $classname, string $identifier) : bool;
	
	/**
	 * Gets the given record for the given namespace, classname and identifier.
	 * 
	 * @param string $namespace
	 * @param string $classname
	 * @param string $identifier
	 * @return RecordInterface
	 * @throws InvalidArgumentException if such record does not exists
	 */
	public function getRecord(string $namespace, string $classname, string $identifier) : RecordInterface;
	
}
