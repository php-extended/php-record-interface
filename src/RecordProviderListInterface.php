<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-record-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Record;

use Countable;
use Iterator;
use Stringable;

/**
 * RecordProviderInterface interface file.
 * 
 * This interface creates an iterable and countable list of RecordProviders.
 * 
 * @author Anastaszor
 * @extends \Iterator<int, RecordProviderInterface>
 */
interface RecordProviderListInterface extends Countable, Iterator, Stringable
{
	
	/**
	 * {@inheritDoc}
	 * @see \Iterator::current()
	 */
	public function current() : RecordProviderInterface;
	
}
